syntax on
set number
set mouse=a " bug
set tabstop=4
set shiftwidth=4
set autochdir
set suffixesadd+=.m
" set cursorline
set nocompatible
set wildmenu

filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
Bundle 'Align'
Bundle 'kien/ctrlp.vim'
Bundle 'othree/html5.vim'
Bundle 'scrooloose/syntastic'
Bundle 'pangloss/vim-javascript'
Bundle 'altercation/vim-colors-solarized' 

filetype plugin indent on

set t_Co=256
syntax enable
set background=light
let g:solarized_termcolors=256
colorscheme solarized

let g:ctrlp_clear_cache_on_exit=0
let g:ctrlp_working_path_mode='ra'
let g:ctrlp_max_files=1000
let g:ctrlp_max_height=1
" Reduce update
" git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle

