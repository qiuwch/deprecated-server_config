# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions

MATLAB_DIR=( /tool/matlab/bin /usr/local/MATLAB/R2012a/bin/ )
for i in ${MATLAB_DIR[*]}
do 
	if [ -d $i ]
	then
		export PATH=$PATH:$i
	fi
done

export EDITOR=vim

stty -ixon 
# Enable ctrl-s : isearch for terminal
alias ls='ls --color'
alias grep='grep --color'
alias dus='du * -sh'
