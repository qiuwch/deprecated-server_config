#!/bin/bash
mkdir -p ~/workspace
for i in $(cat dotfiles.txt);
do 
	if [ -f ~/$i ]
	then
		echo "Warning: file " $i " already exist, skip."
	else
		ln -s `pwd`/dotfiles/$i ~/$i 
		echo "Link file " $i 
	fi
done
